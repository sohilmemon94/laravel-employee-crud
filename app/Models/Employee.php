<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Employee extends Model
{
    use HasFactory;
    use Sortable;

    protected $table = 'employees';
    protected $guarded = [];
    public $timestamps = true;

    protected $fillable = [
        'id',
        'first_name',
        'last_name',
        'email',
        'phone',
        'image',
        'exp',
        'created_at'
    ];

    public $sortable = ['id', 'first_name', 'last_name','email', 'phone','image'];
}
