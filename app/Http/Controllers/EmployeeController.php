<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use File;
use Illuminate\Support\Facades\Auth;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function employesearch(Request $request)
    {

        if($request->ajax())
        {
            $output="";
        //dd($request->search);
            $employees = Employee::where('user_id', Auth::id())
                ->where('first_name','LIKE','%'.$request->search."%")
                ->orWhere('last_name','LIKE','%'.$request->search."%")
                ->orWhere('email','LIKE','%'.$request->search."%")
                ->sortable()
                ->paginate(10);
                if($employees)
                {
                    $i = 1;
                    foreach ($employees as $key => $employee) {
                        $output.='<tr><td>'.$i.'</td><td><img src="'. url('Image/'.$employee->image) .'"
                        style="height: 50px; width: 50px;"></td><td>'.$employee->first_name.'</td>'.
                        '<td>'.$employee->email.'</td>'.
                        '<td ><div class="btn-group btn-group-sm"> <a href="'.url('employee/view').'/'.$employee->id.'" class="btn btn-info">
                        <i class="fas fa-eye  fa-lg"></i>
                    </a><a href="'.url('employee/edit').'/'.$employee->id.'"  class="btn btn-info"><i class="fas fa-edit  fa-lg"></i></a><a href="'.url('employee/delete').'/'.$employee->id.'" class="btn btn-danger delete-confirm"><i class="fas fa-trash"></i></a></div></td>'.
                        '</tr>';

                        $i++;
                    }

                    return Response($output);
                }
        }

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::where('user_id', Auth::id())
                    ->sortable()
              		->paginate(10);


        return view('employee.list', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employee.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'first_name' => 'required|min:3|max:10',
            'last_name' => 'required|min:3|max:10',
            'phone' => 'required|min:10|max:13|unique:employees,phone',
            'email' => 'required|unique:employees,email,',
            'image' => 'required',
            'exp' => 'required'

        ]);


           $employee = new Employee();
           $employee->user_id =  Auth::id();
           $employee->first_name =  $request->first_name;
           $employee->last_name =  $request->last_name;
           $employee->phone =  $request->phone;
           $employee->email =  $request->email;
           if($request->file('image')){
            $file= $request->file('image');
            $filename= date('YmdHi').$file->getClientOriginalName();
            $file-> move(public_path('Image'), $filename);
            $employee->image = $filename;
            }
            $employee->exp = implode(',', $request->exp);
            $employee->save();

           return redirect()->route('list.employee.post')
                    ->with('success','Employee successfully create.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employees = Employee::find($id);

        return view('employee.view', compact('employees'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employees = Employee::find($id);

        return view('employee.edit', compact('employees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'first_name' => 'required|min:3|max:10',
            'last_name' => 'required|min:3|max:10',
            'phone' => 'required|min:10|max:13|unique:employees,phone,'.$id,
            'email' => 'required|unique:employees,email,'.$id,

            'exp' => 'required'
        ]);
           $employees = Employee::find($id);
           $employees->first_name =  $request->input('first_name');
           $employees->last_name =  $request->input('last_name');
           $employees->phone =  $request->input('phone');
           $employees->email =  $request->input('email');
           if(isset($request['image'])){

            if(!empty($employees->image)){
                $image1 = public_path("Image/".$employees->image);

                if (File::exists($image1)) {

                    unlink($image1);
                }
            }

             $image = $request->file('image');
            $image_name = date('YmdHi').$image->getClientOriginalName();


            $image->move(public_path('/Image'),$image_name);
            $employees->image = $image_name;


        }
        //    if($request->file('image')){
        //     $file= $request->file('image');
        //     $filename= date('YmdHi').$file->getClientOriginalName();
        //     $file-> move(public_path('Image'), $filename);
        //     $employees->image = $filename;
        //     }
            $employees->exp = implode(',', $request->exp);
           $employees->update();

           return redirect()->route('list.employee.post')
                    ->with('success','Employee Record successfully update.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Employee::where('id', $id)->delete();
        return redirect()->route('list.employee.post')->with('success','Employee Record deleted successfully.');
    }
}
