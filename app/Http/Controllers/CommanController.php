<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Response;
use Redirect;
use App\Models\City;
use App\Models\User;

class CommanController extends Controller
{

    public function fetchCity(Request $request)
    {
        $data['cities'] = City::where("state_id",$request->state_id)->get(["id","city_name"]);
        return response()->json($data);
    }

    public function checkemail(Request $request)
        {
          //dd($request);
          // $id = $request->id;
           $email = $request->email;
           $rules = array('email' => 'unique:users');

            $validator = Validator::make($request->all(), $rules);

           if ($validator->fails()) {
                echo "false";
            } else {
                echo "true";
            }


        }

        public function checkusername(Request $request)
        {

           $name = $request->name;
           $rules = array('name' => 'unique:users');

            $validator = Validator::make($request->all(), $rules);

           if ($validator->fails()) {
                echo "false";
            } else {
                echo "true";
            }


        }
}
