<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Practical | Registration Page</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ URL::asset('plugins/fontawesome-free/css/all.min.css') }}">
      <!-- icheck bootstrap -->
      <link rel="stylesheet" href="{{ URL::asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
      <!-- Theme style -->
      <link rel="stylesheet" href="{{ URL::asset('dist/css/adminlte.min.css') }}">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="#"><b>Practical</a>
  </div>

  <div class="card">
    <div class="card-body register-card-body">
      <p class="login-box-msg">Register a new member</p>

      <form action="{{ route('register') }}" method="post" id="Registre">
        @csrf
        <div class="input-group mb-3">
            <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" autocomplete="first_name" autofocus placeholder="First Name">

          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
          @error('first_name')
                                    <span class="invalid-feedback first_name" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
        </div>
        <div class="input-group mb-3">
            <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}" autocomplete="last_name" autofocus placeholder="Last Name">

            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
            @error('last_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
        </div>
        <div class="input-group mb-3">
            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}"  autocomplete="name" autofocus placeholder="User Name">

            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
            @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
        </div>
        <div class="input-group mb-3">
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  autocomplete="email" placeholder="Email">

          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
          @error('email')
          <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
          </span>
        @enderror
        </div>
        <div class="input-group mb-3">
        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="new-password"  placeholder="Password">

          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
          @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
        </div>
        <div class="input-group mb-3">
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation"  autocomplete="new-password"  placeholder="Confirm Password">

          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
            <select id="state_id" name="state_id" class="custom-select rounded-0  @error('state_id') is-invalid @enderror" id="exampleSelectRounded0">
                <option value="" selected>Select State</option>
                        @foreach($states as $state)
                                <option value="{{ $state->id }}">{{ $state->state_name }}</option>
                            @endforeach

              </select>
              @error('state_id')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div>
        <div class="input-group mb-3">
            <select   id="city_id" name="city_id" class="custom-select rounded-0 @error('city_id') is-invalid @enderror"  id="exampleSelectRounded0">
                <option value="" selected>Select City</option>

              </select>
              @error('city_id')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div>
        <div class="row">
          <div class="col-4">

          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Register</button>
          </div>
          <!-- /.col -->
        </div>
      </form>


    <br/>
      <a href="{{ route('login') }}" class="text-center">I already have a member</a>
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->

<!-- jQuery -->
{{-- <script src="{{ URL::asset('plugins/jquery/jquery.min.js')}}"></script> --}}

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <!-- Bootstrap 4 -->
      <script src="{{ URL::asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
      <!-- AdminLTE App -->
      <script src="{{ URL::asset('dist/js/adminlte.min.js') }}"></script>
      <script src="{{ URL::asset('js/custom.js') }}"></script>
      <script type="text/javascript">
      jQuery(document).ready(function(){
        $('#state_id').on('change', function () {
                var idState = this.value;
                $("#city_id").html('');
                $.ajax({
                    url: "{{url('api/fetch-cities')}}",
                    type: "POST",
                    data: {
                        state_id:  jQuery('#state_id').find(':selected').val(),
                        _token: '{{csrf_token()}}'
                    },
                    dataType: 'json',
                    success: function (res) {
                        console.log(res);
                        $('#city_id').html('<option value="">Select City</option>');
                        $.each(res.cities, function (key, value) {
                            $("#city_id").append('<option value="' + value
                                .id + '">' + value.city_name + '</option>');
                        });
                    }
                });
            });

        });
      </script>
      <script>
        $("#Registre").validate({
            rules: {
                first_name: {
                          required: true,

                      },
                      last_name: {
                          required: true,

                      },
                phone: {
                          required: true,
                          minlength: 10,
                          maxlength: 13,
                      },
                email: {
                          required: true,
                          email: true,
                        valid_email: true,
                          remote: {
                          url: '{{ url('register/email') }}',
                          type: "post",
                          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                          data: {
                              email: function() {
                                  return $('#Registre :input[name="email"]').val();
                              },
                          }
                        },
                      },
                name: {
                          required: true,
                          remote: {
                          url: '{{ url('register/username') }}',
                          type: "post",
                          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                          data: {
                            name: function() {
                                  return $('#Registre :input[name="name"]').val();
                              },
                          }
                        },
                      },
                password: {
                          required: true
                      },
                      city_id: {
                          required: true
                      },
                      state_id: {
                          required: true
                      },


            },
            messages: {
                first_name: {
                    required: "Please Enter First Name"

                },
                last_name: {
                    required: "Please Enter Last Name",

                },
                phone: {
                    required: "Please Enter Mobile Number",
                    minlength: "Mobile Number Minimum 3 latter",
                    maxlength: "Mobile Number Maximum 13 latter"
                },
                email: {
                    required: "Please Enter Email.",
                    remote: "This email address allready register!",
                    email: "Enter Proper Email Address!",
                },
                name: {
                    required: "Please Enter Username",
                    remote: "This Username allready register!"
                },

                city_id: {
                    required: "Please Select City Name"
                },

                state_id: {
                    required: "Please Select State Name"
                },
                password: {
                    required: "Please Enter Password"
                },


            },
        });
    </script>
</body>
</html>
