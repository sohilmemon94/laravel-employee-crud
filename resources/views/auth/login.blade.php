<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Practical | Login</title>
      <!-- Google Font: Source Sans Pro -->
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
      <!-- Font Awesome -->
      <link rel="stylesheet" href="{{ URL::asset('plugins/fontawesome-free/css/all.min.css') }}">
      <!-- icheck bootstrap -->
      <link rel="stylesheet" href="{{ URL::asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
      <!-- Theme style -->
      <link rel="stylesheet" href="{{ URL::asset('dist/css/adminlte.min.css') }}">
   </head>
   <body class="hold-transition login-page">
      <div class="login-box">
         <!-- /.login-logo -->
         @if ($message = Session::get('success'))
         <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('error'))
         <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if (count($errors) > 0)
         <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         <div class="card card-outline card-primary">
            <div class="card-header text-center">
               <a href="#" class="h1"><b>Practical</a>
            </div>
            <div class="card-body">
               <p class="login-box-msg">Sign in to start your session</p>
               <form method="POST" action="{{ route('login') }}" id="loginForm">
                  @csrf
                  <div class="input-group mb-3">
                     <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" required autofocus placeholder="E-Mail Address or Username">


                     <div class="input-group-append">
                        <div class="input-group-text">
                           <span class="fas fa-envelope"></span>
                        </div>
                     </div>
                     @error('email')
                     <span class="invalid-feedback email" role="alert">
                     <strong>{{ $message }}</strong>
                     </span>
                     @enderror
                     {{-- <div class="invalid-feedback email"></div> --}}
                  </div>
                  <div class="input-group mb-3">
                     <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">

                     <div class="input-group-append">
                        <div class="input-group-text">
                           <span class="fas fa-lock"></span>
                        </div>
                     </div>
                     @error('password')
                     <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                     </span>
                     @enderror
                     <div class="invalid-feedback password"></div>
                  </div>
                  <div class="row">
                     <div class="col-4">
                     </div>
                     <!-- /.col -->
                     <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                     </div>
                     <!-- /.col -->
                  </div>
               </form>
               <div class="row">
                  <br/>
                  <div class="col-8">
                     {{-- <p class="mb-1">
                        <br/>
                        @if (Route::has('password.request'))
                        <a  href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                        </a>
                        @endif
                     </p> --}}
                     <p class="mb-0">
                        <br/>
                        <a href="{{ route('register') }}" class="text-center">Register a new member</a>
                      </p>
                  </div>
               </div>
            </div>
            <!-- /.card-body -->
         </div>
         <!-- /.card -->
      </div>
      <!-- /.login-box -->
      <!-- jQuery -->
      <script src="{{ URL::asset('plugins/jquery/jquery.min.js')}}"></script>
      <!-- Bootstrap 4 -->
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
      <script src="{{ URL::asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
      <!-- AdminLTE App -->
      <script src="{{ URL::asset('dist/js/adminlte.min.js') }}"></script>
      <script>
        $("#loginForm").validate({
            rules: {
                email: {
                          required: true,


                      },
                      password: {
                          required: true

                      },
            },
            messages: {
                email: {
                      required: "Please Enter Your Email or Username"

                  },
                  password: {
                      required: "Please Enter Your Password",

                  },

            },
        });
    </script>
   </body>
</html>
