@extends('layouts.app')

@section('content')
  <!-- Content Wrapper. Contains page content -->

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Employee Detail</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ url('/home') }}">Dashboard</a></li>
              <li class="breadcrumb-item active">Employee Detail</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Employee Detail</h3>

          {{-- <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div> --}}
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">
                <div class="row">
                    <div class="col-12">

                        <div class="post">
                          <div class="user-block">
                            <img class="img-circle img-bordered-sm" src="{{ url('Image/'.$employees->image) }}" alt="user image">
                            <span class="username">
                              <a href="#">{{$employees->first_name}} {{$employees->last_name}}</a>
                            </span>
                            <span class="description">Email - {{$employees->email}}</span>
                            <span class="description">Phone - {{$employees->phone}}</span>
                          </div>
                          <!-- /.user-block -->
                          <p>

                          </p>
                        </div>


                    </div>
                  </div>
                  @if (count(explode(',', $employees->exp)) > 0)
              <div class="row">
                <div class="col-12 col-sm-12"><h5 class="mt-5 text-muted">Experience</h5></div>
                @foreach(explode(',', $employees->exp) as $key => $val)
                <div class="col-12 col-sm-4">
                  <div class="info-box bg-light">
                    <div class="info-box-content">
                      <span class="info-box-number text-center text-muted mb-0">{{$val}}</span>
                    </div>
                  </div>
                </div>
                @endforeach

              </div>
              @endif

            </div>

          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->

  <!-- /.content-wrapper -->
@endsection
