@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    @if ($message = Session::get('success'))
                  <div class="alert alert-success alert-block">
                      <button type="button" class="close" data-dismiss="alert">×</button>
                      <strong>{{ $message }}</strong>
                  </div>
                  @endif
                  @if (count($errors) > 0)
                  <div class="alert alert-danger">
                      <strong>Whoops!</strong> There were some problems with your input.
                      <ul>
                          @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
                  @endif
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Employee Form</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ url('/home') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">Employee Add Form</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-2">


        </div>
        <div class="col-md-8">
            <!-- general form elements disabled -->
          <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title">Employee Add Form</h3>
            </div>
            <!-- /.card-header -->
              <form action="{{ route('update.employee.post', $employees->id) }}" id="editEmployee" method="POST" enctype="multipart/form-data">
              @csrf
              <input type="hidden" id="id" name="id" value="{{$employees->id}}">
              <div class="card-body">
                <div class="row">
                       <div class="col-4">
                           <div class="form-group">
                               <label for="exampleInputEmail1">First Name</label>
                               <input type="text" class="form-control" id="DistrictInputName" value="{{ $employees->first_name}}" name="first_name" placeholder="First Name">
                           </div>
                       </div>
                       <div class="col-4">
                           <div class="form-group">
                               <label for="exampleInputEmail1">Last Name</label>
                               <input type="text" class="form-control" id="DistrictInputName" value="{{ $employees->last_name}}" name="last_name" placeholder="Last Name">
                           </div>
                       </div>
                       <div class="col-4">
                           <div class="form-group">
                               <label for="exampleInputEmail1">Email</label>
                               <input type="email" class="form-control" id="DistrictInputName" value="{{ $employees->email}}" name="email" placeholder="Email">
                           </div>
                       </div>
                </div>
                <div class="row">
                   <div class="col-4">
                       <div class="form-group">
                           <label for="exampleInputEmail1">Phone</label>
                           <input type="number" class="form-control" id="DistrictInputName" value="{{ $employees->phone}}" name="phone" placeholder="Phone">
                       </div>
                   </div>
                   <div class="col-8">
                       <div class="form-group">
                           <label for="exampleInputFile">Employee Image</label>
                           <img src="{{ url('Image/'.$employees->image) }}" style="height: 30px; width: 30px;">
                           <div class="input-group">
                               <div class="custom-file">
                                   <input type="file" id="profile_image" name="image" accept="image/png, image/gif, image/jpeg" placeholder="Select Image" class="custom-file-input">
                                   <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                               </div>

                             </div>

                       </div>
                   </div>
            </div>
            <div class="row">
               <div class="col-8">
                   <div class="form-group " id="dynamicAddRemove">
                       <label for="exampleInputEmail1">Experience</label>
                       <button type="button" name="add" id="dynamic-ar" class="btn btn-outline-primary">Add Experience</button>
                       @foreach(explode(',', $employees->exp) as $key => $val)

                            @if($key == 0)
                            <input type="text" class="form-control" id="DistrictInputName" name="exp[]" value="{{$val}}" placeholder="Experience">
                        @else
                        <div class="added"><input type="text" name="exp[]" placeholder="Experience" value="{{$val}}" class="form-control" /><button type="button" class="btn btn-outline-danger remove-input-field">Delete</button></div>
                            @endif
                       @endforeach

                   </div>
               </div>
               <div class="col-1">
                   <div class="card-tools">

                     </div>
               </div>
           </div>


             </div>
             <div class="card-footer">
                     <button type="submit" class="btn btn-primary">Update</button>
             </div>

              </form>

            <!-- /.card-body -->
          </div>
          <!-- /.card -->


        </div>
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-2">


        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
  <script>
  $("#editEmployee").validate({
      rules: {
          first_name: {
                    required: true,
                },
        last_name: {
                    required: true
                },
        email: {
            required: true,
            email: true,
            valid_email: true
        },
        phone: {
            required: true
        },

        'exp[]': {
            required: true
        }
      },
      messages: {
        first_name: {
              required: "Please Enter First Name"
          },
          last_name: {
              required: "Please Enter Last Name",

          },
          email: {
                      required: "Please Enter Your Email",
                      email: "Enter Proper Email Address!",


                  },
          phone: {
              required: "Please Enter Phone Number",

          },

          'exp[]': {
              required: "Please Enter Experience",

          },

      },
  });
  var i = 0;
        $("#dynamic-ar").click(function () {

            ++i;
            $("#dynamicAddRemove").append('<tr><td><input type="text" name="exp[]" placeholder="Experience" class="form-control" /></td><td><button type="button" class="btn btn-outline-danger remove-input-field">Delete</button></td></tr>'
                );
        });
        $(document).on('click', '.remove-input-field', function () {

        $(this).parents('.added').remove();
    });
    $(document).on('click', '.remove-input-field', function () {
        $(this).parents('tr').remove();
    });
</script>
@endsection
