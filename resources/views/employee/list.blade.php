@extends('layouts.app')

@section('content')
 <!-- Content Header (Page header) -->
 <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">

        <div class="col-sm-6">
          <h1>Employee List</h1>
          @if ($message = Session::get('success'))
                  <div class="alert alert-success alert-block">
                      <button type="button" class="close" data-dismiss="alert">×</button>
                      <strong>{{ $message }}</strong>
                  </div>
                  @endif
                  @if (count($errors) > 0)
                  <div class="alert alert-danger">
                      <strong>Whoops!</strong> There were some problems with your input.
                      <ul>
                          @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
                  @endif
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ url('/home') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">Employee List</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">

          <!-- /.card -->

          <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-8"> <h3 class="card-title">Employee List</h3></div>
                    <div class="col-2">
                      <form class="form-inline">
                          <div class="input-group input-group-sm">
                              <input class="form-control " type="search" placeholder="Search By Name" id="employe_name" aria-label="Search">
                          </div>
                      </form>
                    </div>
                    <div class="col-2"><a href="{{ url('employee/add') }}" class="btn btn-block bg-gradient-primary btn-xs">Employee Add</a></div>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              @if (count($employees) > 0)
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>@sortablelink('id', 'No')</th>
                  <th>@sortablelink('image', 'Image')</th>
                  <th>@sortablelink('first_name', 'First Name')</th>
                  <th>@sortablelink('email', 'Email')</th>
                  <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @php $i = ($employees->currentpage()-1)* $employees->perpage() + 1;@endphp
                @foreach ($employees as $key => $employee)
                <tr>
                  <td> {{$i}} </td>
                  <td><img src="{{ url('Image/'.$employee->image) }}"
                    style="height: 50px; width: 50px;"></td>
                  <td>{{$employee->first_name}}</td>
                  <td>{{$employee->email}}</td>

                  <td><div class="btn-group btn-group-sm">
                    <a href="{{ url('employee/view') }}/{{$employee->id}}" class="btn btn-info">
                        <i class="fas fa-eye  fa-lg"></i>
                    </a><a href="{{ url('employee/edit') }}/{{$employee->id}}" class="btn btn-info">
                          <i class="fas fa-edit  fa-lg"></i>
                      </a>
                      <a href="{{ url('employee/delete') }}/{{$employee->id}}" class="btn btn-danger delete-confirm">
                          <i class="fas fa-trash "></i>
                      </a></div></td>
                </tr>
                @php  $i += 1; @endphp
                @endforeach

                </tbody>

              </table>
              {!! $employees->links() !!}
              @else
              <div class="row">
                  <div class="col-4"></div>
                  <div class="col-4"> <h3 class="card-title">Employee List Not Found Please Add Employee</h3></div>
                  <div class="col-4"></div>
              </div>
              @endif
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <script>
    $('.delete-confirm').on('click', function (event) {
      event.preventDefault();
      const url = $(this).attr('href');
      swal({
          title: 'Are you sure?',
          text: 'This record and it`s details will be permanantly deleted!',
          icon: 'warning',
          buttons: ["Cancel", "Yes!"],
      }).then(function(value) {
          if (value) {
              window.location.href = url;
          }
      });
  });
  jQuery('#employe_name').on('keyup',function(e){
  $value=$(this).val();
  e.preventDefault();
  jQuery.ajaxSetup({
     headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
  });
  jQuery.ajax({
     url: "{{ url('employee/search') }}",
     method: 'post',
     data: {
      search: $value
        //id: jQuery('#current_customer').val()
     },
     success: function(data){


      jQuery('tbody').html(data);

      console.log(data);

       }

     });
  });
  </script>
@endsection
