<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CommanController;
use App\Http\Controllers\EmployeeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/register', function () {
    return view('auth.register');
});
Route::post('api/fetch-cities', [CommanController::class, 'fetchCity']);
Route::post('register/email',  [CommanController::class, 'checkemail']);
Route::post('register/username',  [CommanController::class, 'checkusername']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

/*--------Employee Start---------*/
Route::get('employee/list', [EmployeeController::class, 'index'])->name('list.employee.post');
Route::get('employee/add', [EmployeeController::class, 'create'])->name('view.employee.post');
Route::post('employee/stores', [EmployeeController::class, 'store'])->name('store.employee.post');
Route::get('employee/view/{id}', [EmployeeController::class, 'show']);
Route::get('employee/edit/{id}', [EmployeeController::class, 'edit']);
Route::post('employee/edit/{id}', [EmployeeController::class, 'update'])->name('update.employee.post');
Route::get('employee/delete/{id}', [EmployeeController::class, 'destroy'])->name('destroy.employee.post');
Route::post('employee/search',  [EmployeeController::class, 'employesearch']);

